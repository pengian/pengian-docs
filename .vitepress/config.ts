import { defineConfig } from "vitepress";
import { mdPlugin } from "./plugins";

export const PUBLIC_PATH = '/pengian-docs/FE';

export default defineConfig({
  srcDir: "src",
  title: "Pengian Docs",
  description: "个人技术文档",
  base: PUBLIC_PATH,
  lang: "zh",
  outDir: "docs-fe",
  head: [
    [
      "link",
      {
        rel: "icon",
        href: PUBLIC_PATH + "favicon.ico",
      },
    ],
    [
      "meta",
      {
        name: "referrer",
        content: "no-referrer",
      },
    ],
  ],
  themeConfig: {
    logo: PUBLIC_PATH + "pengian.jpg", // 左上角标题的logo
    // 右上角导航栏
    nav: [
      { text: "首页", link: "/" },
      {
        text: "Vue",
        items: [
          { text: "Vue2", link: "/vue/vue2/" },
          { text: "Vue3", link: "/vue/vue3/" },
        ],
      },
      {
        text: "JS & TS",
        items: [
          {
            text: "JS",
            link: "/js/js/",
          },
          {
            text: "TS",
            link: "/js/ts/",
          },
        ],
      },
      {
        text: "Css",
        link: "/css/",
      },
      {
        text: "可视化",
        items: [
          { text: "Echarts", link: "/visualization/echarts/" },
          { text: "Canvas", link: "/visualization/canvas/" },
          { text: "Svg", link: "/visualization/svg/" },
        ],
      },
      {
        text: "Plugin",
        items: [{ text: "vsCode插件", link: "/plugin/vsCode/" }],
      },
      {
        text: "工程化",
        items: [
          {
            text: "构建工具",
            items: [
              { text: "Webpack", link: "/build/webpack/" },
              { text: "Rollup", link: "/build/rollup/" },
            ],
          },
          {
            text: "MonoRepo",
            items: [
              { text: "Lerna", link: "/build/lerna/" },
              { text: "Pnpm", link: "/build/pnpm/" },
            ],
          },
        ],
      },
      {
        text: "Hybrid App",
        items: [
          {
            text: "APP",
            link: "/hybrid/app/",
          },
          {
            text: "Flutter",
            link: "/hybrid/flutter/",
          },
          {
            text: "客户端",
            link: "/hybrid/client/",
          },
        ],
      },
      {
        text: "服务端",
        link: "/server/",
      },
      {
        text: "工具",
        items: [
          {
            text: "Whistle",
            link: "/tools/whistle/",
          },
          {
            text: "Axios",
            link: "/tools/axios/",
          },
          {
            text: "AntDesign",
            link: "/tools/ant-design/",
          },
        ],
      },
      {
        text: "外部链接",
        items: [
          {
            text: "Vue",
            items: [
              { text: "Vue 2.x", link: "https://cn.vuejs.org/" },
              { text: "Vue Cli", link: "https://cli.vuejs.org/zh/guide/" },
              { text: "Vue Router", link: "https://router.vuejs.org/zh/" },
              { text: "Vuex", link: "https://vuex.vuejs.org/zh/" },
            ],
          },
          {
            text: "其它",
            items: [
              {
                text: "Element UI",
                link: "https://element.eleme.cn/#/zh-CN/component/installation",
              },
              { text: "axios", link: "http://www.axios-js.com/zh-cn/docs/" },
              { text: "Express", link: "https://www.expressjs.com.cn/" },
            ],
          },
        ],
      },
    ],
    sidebar: {
      "/vue/vue2/": [
        {
          text: "Vue2",
          items: [
            { text: "知识点", link: "/vue/vue2/note" },
            {
              text: "应用技巧",
              link: "/vue/vue2/",
            },
            {
              text: "指令集",
              link: "/vue/vue2/directive",
            },
          ],
        },
        {
          text: "Vue2应用进阶系列",
          items: [
            {
              text: "组件内的发布/订阅模式",
              link: "/vue/vue2/blog/pub-sub",
            },
            {
              text: "组件封装及复用",
              link: "/vue/vue2/blog/component",
            },
            {
              text: "生命周期的秘密",
              link: "/vue/vue2/blog/lifecycle",
            },
          ],
        },
      ],
      "/visualization/echarts/": [
        {
          text: "Echarts",
          items: [
            {
              text: "入门教程",
              link: "/visualization/echarts/",
            },
            {
              text: "常见案例",
              link: "/visualization/echarts/example",
            },
            {
              text: "FAQ",
              link: "/visualization/echarts/faq",
            },
          ],
        },
      ],
      "/visualization/svg/": [
        {
          text: "Svg",
          items: [
            {
              text: "svg基础",
              link: "/visualization/svg/",
            },
            {
              text: "常见应用",
              link: "/visualization/svg/apply/",
            },
          ],
        },
      ],
      "/hybrid/flutter/": [
        {
          text: "Flutter",
          items: [
            {
              text: "入门教程",
              link: "/hybrid/flutter/",
            },
            {
              text: "Dart",
              link: "/hybrid/flutter/dart/",
            },
          ],
        },
      ],
    },
  },
  markdown: {
    toc: { includeLevel: [2, 3] },
    theme: "material-palenight",
    config: (md) => mdPlugin(md),
  },
});
