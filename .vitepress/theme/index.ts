import { globals } from '../components/index'
import DefaultTheme from 'vitepress/theme'
import ElementPlus from 'element-plus'
import 'element-plus/dist/index.css'
import './base.less'


export default {
  ...DefaultTheme,
  enhanceApp: ({ app }: any) => {
    app.use(ElementPlus)
    if (typeof window != 'undefined') {
      //@ts-ignore
      import('./iconpark.js')
    }
    globals.forEach(([name, Comp]) => {
      app.component(name, Comp)
    })
  }
}
