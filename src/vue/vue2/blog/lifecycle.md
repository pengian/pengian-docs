## 前言

&emsp;&emsp;本篇幅主要讲述Vue组件生命周期的源码分析，都过分析源码来深入了解Vue组件实例化的秘密；

&emsp;&emsp;学完整篇文章，你可能收获到：

- Vue组件的生命周期分析


### 生命周期图例

![生命周期图例](https://v2.cn.vuejs.org/images/lifecycle.png)
