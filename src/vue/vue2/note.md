# 知识点

## 渲染函数

​ [渲染函数](https://cn.vuejs.org/v2/guide/render-function.html)

提到渲染函数就不由得想起 VNode，Vue 将渲染函数转换为 VNode，VNode 是一个对象，包含了节点名称、属性、子节点等信息，不管是单文件组件(SFC)，还是说 JSX 组件、render 函数、函数组件，其最终的编译结果都是 VNode 数组(VNode 是虚拟 DOM 的组成单位)。

官网中介绍渲染函数的例子中，提到了动态组件，类似于内置的`component`组件(其实不是类似，component 组件就是组件式的渲染函数，毕竟 SFC 最终输出的还是一个标准的 render 函数)，根据这个动态组件实现方式，常见的一些表单、表格组件就可以使用这种方式来实现业务封装，如：

```js
Vue.component("dynamicForm", {
  data() {
    return {
      formList: [
        { comp: "ElInput", props: { type: "text" } },
        {
          comp: "ElButton",
          props: { type: "primary" },
          on: { click: this.handleSubmit },
        },
      ],
    };
  },
  render(createElement) {
    return createElement(
      "div",
      {},
      this.formList.map((item) => {
        return createElement(item.comp, {
          props: item.props || {},
          on: item.on || {},
        });
      })
    );
  },
});
```
