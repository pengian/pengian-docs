# whistle
[[toc]]

## 简介

### 什么是whistle？

​		whistle是一个基于Node实现的跨平台web调试代理工具，类似的工具有Windows平台上的[Fiddler](http://www.telerik.com/fiddler/)，主要用于查看、修改HTTP、HTTPS、Websocket的请求、响应，也可以作为HTTP代理服务器使用，不同于Fiddler通过断点修改请求响应的方式，whistle采用的是类似配置系统hosts的方式，一切操作都可以通过配置实现，支持域名、路径、正则表达式、通配符、通配路径等多种[匹配方式](http://wproxy.org/whistle/pattern.html)，且可以通过Node模块[扩展功能](http://wproxy.org/whistle/plugins.html)



### whistle功能

​		whistle的功能十分强大，除了前端同学常用的抓包之外，还支持脚本注入、篡改请求、文件替换、代理转发等等功能，以下是其功能大全的思维导图，可以看到，它除了我刚提到的功能外，甚至可以扩展插件来实现更复杂的调试情景

![img](https://gitee.com/Pengian/picgo/raw/master/images/1277687-20191107130411870-1031115488.png)







## 安装启用

### whistle安装

版本要求：

![Nodejs](https://img.shields.io/badge/nodejs->=0.10.0-brightgreen.svg)

~~~bash
npm i -g whistle
# 或使用yarn
yarn add -g whistle
~~~



### 启动

默认配置启动：

~~~bash
# 启动，默认8899端口启动，可加-p参数指定端口
w2 start
# 停止
w2 stop
# 重启
w2 restart
~~~

启动成功：

![](https://gitee.com/Pengian/picgo/raw/master/images/20210907093634.png)

### 设置代理服务器

#### 全局设置

- window10系统下，可直接搜索"更改代理设置"，然后手动打开“代理”，填入whistle的默认代理地址`127.0.0.1`，端口`8899`，如下：

![image-20210906205407073](https://gitee.com/Pengian/picgo/raw/master/images/image-20210906205407073.png)





#### 局部设置

除了使用系统的全局代理外，还可以利用插件来实现单浏览器的代理设置，[SwitchyOmega](https://addons.mozilla.org/zh-CN/firefox/addon/switchyomega/?utm_source=addons.mozilla.org&utm_medium=referral&utm_content=search)

![image-20210907094039073](https://gitee.com/Pengian/picgo/raw/master/images/image-20210907094039073.png)

首次设置，应点击“选项”，进入插件设置界面，设置如下配置并点击左下角的"应用"

![image-20210907094915167](https://gitee.com/Pengian/picgo/raw/master/images/image-20210907094915167.png)

### whistle配置页面

#### 规则界面

​		浏览器打开`127.0.0.1:8899`(如启动时指定的端口，请将8899端口改为启动时候指定的端口)，进入whistle配置页面，默认进入`规则配置`界面，默认只能同时开启默认配置+一项项目配置，可点击顶部菜单栏的`Settings`应用里面的`Use multiple rules`来激活多规则配置，

![image-20210907104356703](https://gitee.com/Pengian/picgo/raw/master/images/image-20210907104356703.png)



#### 抓包界面

​		抓包界面，不管是手机、平板、PC，需要抓包https请求的话，都需要在这些设备上安装并信任whistle提供的根证书，相关的证书安装步骤可参考[whistle官网](http://wproxy.org/whistle/install.html)

<img src="https://gitee.com/Pengian/picgo/raw/master/images/image-20210907104912914.png" alt="image-20210907104912914"  />



#### 变量值界面

​		变量值界面，在变量值内写入js语句、css语句，使用时，会以script、style标签的形式注入到网站中，在规则配置中使用`{变量名}`的形式引用变量值，变量值同样支持新建、导入/导出等操作，

![image-20210907105105645](https://gitee.com/Pengian/picgo/raw/master/images/image-20210907105105645.png)



## whistle常用配置

### 配置host

​		常用的场景是github设置host提升网站访问速度(查具体ip略微麻烦)，配置规则如下：

~~~bash
# 配置host，与其它配置规则不同，host配置需要将ip写在左侧，操作地址写在右侧
20.205.243.166 github.com
~~~



### 移动端调试

#### vConsole注入

​		在项目代码中显式注入vConsole之类的调试工具十分麻烦，稍有不慎还会污染生产环境，whistle的脚本的注入功能就很好的为我们解决这个问题，不管是开发环境，还是说生产环境，whistle都可以通过script、style之类的标签为页面注入相关的调试脚本，大大提高了开发人员的调试效率，配置规则如下：

~~~bash
# 注入移动端调试面板vConsole，下面的规则使用了whistle变量值，需要在变量值界面插件vConsole.js变量
www.pengian.com jsAppend://{vConsole.js}
~~~

​		vConsole.js变量值：

~~~js
(function() {
    var scp = document.createElement('script');
    // 加载最新的mdebug版本
    scp.src = 'https://cdn.bootcdn.net/ajax/libs/vConsole/3.9.1/vconsole.min.js';
    scp.async = true;
    scp.charset = 'utf-8';
    // 加载成功并初始化
    scp.onload = function() {
        new VConsole();
    };
    // 加载状态切换回调
    scp.onreadystate = function() {};
    // 加载失败回调 
    scp.onerror = function() {};
    document.getElementsByTagName('head')[0].appendChild(scp);
})();
~~~



#### weinre

​		远程调试工具weinre十分强大，在移动端调试工具中实用性max，whistle内部支持一键开启weinre，配置规则如下：

~~~bash
# 启动weinre，test标记这项规则启动的weinre，可以省略，点击顶部导航栏的weinre看见刚才启动的test了，手机链接上www.pengian.com即可调试
www.pengian.com weinre://test
~~~



### 代理转发

​		前端开发的日常中，受限于浏览器的同源策略影响，跨域是家常便饭的了，使用代理转发就可以很好的解决跨域的问题，转发跟浏览器层面无关，所有的资源请求都到了代理服务器上，再返回到页面上，而代理服务器怎么去拿到资源，我们不需要关心；配置规则如下：

~~~bash
# 设置反向代理，接口/api前缀的请求都转发到对应的后端服务接口中
# 将www.pengian.com的资源请求都代理到主机的3000端口上，访问www.pengian.com相当于访问3000端口的服务
# 注意，在使用webpack的devServer.proxy时，会有不明的冲突，建议项目中关闭devServer.proxy
www.pengian.com/api http://后端服务地址
www.pengian.com http://127.0.0.1:3000
~~~



### 线上文件替换

​		项目进入测试阶段，某些功能需要前端同学重新打包部署到测试环境，但问题来了，大部分前端同学一般来说，是没服务器权限来操作项目部署的，大多时候得麻烦后端或者运维同学来部署项目，而进入测试阶段的时候，有时候有些bug fix不能很好的定位，或者说某些bug就需要线上部署才能看到效果的，这样就免不了频繁的部署(后端小哥逐渐暴躁……)，使用whistle提供的线上文件替换功能可以将本地打包代码替换线上代码，前端同学借此测试线上环境，复现排查bug，配置规则如下：

~~~bash
# 本地文件替换线上文件
www.pengian.com file://E:\dist\
~~~

​		[weinre官方文档](https://github.com/nupthale/weinre)







## 使用情景


### 使用whistle解决本地环境cookie跨域

[![Nodejs](https://img.shields.io/badge/nodejs->=0.10.0-brightgreen.svg)](http://nodejs.cn/download/)[Whistle](http://wproxy.org/whistle/install.html)

​		项目开发过程中，单点登录功能是通过服务端写入[cookie](https://developer.mozilla.org/zh-CN/docs/Web/HTTP/Headers/Cookie)到客户端，服务端验证cookie来实现的，由于本地开发环境和服务端不是部署在一起的，会违背浏览器的[同源策略](https://developer.mozilla.org/zh-CN/docs/Web/HTTP/CORS)造成跨域，而导致本地开发环境无法正常的调试登录相关的功能；

​		要解决跨域问题，常见的处理方法就是前端同学进行接口代理转发，而现在cookie跨域的问题，需要的就是服务的转发了，实现代理转发功能最常用的工具就是nginx、whistle之类的[代理服务器](https://baike.baidu.com/item/%E4%BB%A3%E7%90%86%E6%9C%8D%E5%8A%A1%E5%99%A8/97996?fr=aladdin)；

​		[whistle](http://wproxy.org/whistle/install.html)是一个跨平台的web调试代理工具，它基于node平台开发，有很好的跨平台性，且安装使用都非常的简单，不需要像nginx之类的代理服务器，需要下载不同的安装包，whistle的安装十分的简单，安装了nodejs只需要`npm install -g whistle`即可完成安装，多平台统一；

​		启动whistle，命令行中使用`w2 start`即可在`localhost:8899`端口启动whistle服务，在进行调试之前，我们需要把相关的代理规则写好，并设置`localhost:8899`为代理服务器；

1. 浏览器打开`localhost:8899`whistle配置页面，找到左侧导航栏的`Rules`；

2. 假设服务端为`http://www.server.com/`，本地服务为`localhost:8080`，则whistle的代理规则应该有：

   ```bash
   # 如需要接口转发，前端接口转发前缀为/api，后端服务接口前缀为/server，则使用以下规则
   http:www.server.com/api http:www.server.com/server
   # 特意使用子路径来避免服务转发和接口转发混淆，通过访问/test来转发到前端的本地服务，也解决了跨域问题
   http:www.server.com/test http://127.0.0.1:8080
   ```

3. 设置好代理规则之后，`Ctrl + S`保存一下，下一步就是将`localhost:8899`设置为代理服务器了，win10电脑可直接使用系统的搜索功能，搜索"<u>代理服务器设置</u>"，手动打开代理服务器，并设置对应的代理地址为`127.0.0.1`，端口为`8899`，或使用chrome插件[SwitchyOmega](https://addons.mozilla.org/zh-CN/firefox/addon/switchyomega/?utm_source=addons.mozilla.org&utm_medium=referral&utm_content=search)；

4. 以上设置好代理服务器和代理规则进行代理转发了，现在就可以打开浏览器访问`http:www.server.com/test`，此时访问的服务就是本地开发环境的服务了



### vite搭配whistle

​	要说当下最热的社区工具，当属vite，vite项目搭配whistle会偶尔出现项目无故刷新的现象，原因是vite的ws服务会不断地尝试链接代理地址，解决这一问题的方法是配置server.hmr.host

```js
// vite.config.js
export default defineConfig(({ command, mode }: ConfigEnv) => {
  return {
    // ...其他配置
    
    server: {
      hmr: {
        protocol: 'ws',
        host: '127.0.0.1'
      }
    }
  };
});
```

参考：

[github issue: vite always refresh and connecting](https://github.com/vitejs/vite/issues/2968)

[掘金文章：Vite + whistle：一劳永逸的开发环境代理方案](https://juejin.cn/post/7065981615994273806)





## 参考

- [whistle学习（一）之安装、使用、软件功能了解](https://www.cnblogs.com/kunmomo/p/11811458.html)

- [利用whistle调试移动端页面](http://imweb.io/topic/5981a34bf8b6c96352a59401)

