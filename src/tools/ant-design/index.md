## ant-design-vue

#### 1、provide和inject

​		利用一个配置文件，采用hook的方式写下provide和inject的配置，provide的key采用Symbol避免与不知层级的provide冲突

```js
import { computed, inject, provide } from 'vue';
import type { Ref, InjectionKey, ComputedRef } from 'vue';

export interface RowContext {
  gutter: ComputedRef<[number, number]>;
  wrap: ComputedRef<boolean>;
  supportFlexGap: Ref<boolean>;
}

export const RowContextKey: InjectionKey<RowContext> = Symbol('rowContextKey');

const useProvideRow = (state: RowContext) => {
  provide(RowContextKey, state);
};

const useInjectRow = () => {
  return inject(RowContextKey, {
    gutter: computed(() => undefined),
    wrap: computed(() => undefined),
    supportFlexGap: computed(() => undefined),
  });
};

export { useInjectRow, useProvideRow };
export default useProvideRow;
```

