# SVG

## 概述

SVG 意为可缩放矢量图形（Scalable Vector Graphics），SVG 使用 XML 格式定义图像；SVG 图像在放大或改变尺寸的情况下其图形质量不会有损失；

### SVG 代码解析：

SVG 代码以 \<svg> 元素开始，包括开启标签 \<svg> 和关闭标签 \</svg> 。这是根元素。width 和 height 属性可设置此 SVG 文档的宽度和高度。version 属性可定义所使用的 SVG 版本，xmlns 属性可定义 SVG 命名空间。

## SVG 与 Canvas 的区别

`SVG`是基于可扩展标记语言XML描述的2D图形的语言；

- 不依赖分辨率，可以无限放大；
- 支持事件处理器；
- 最适合带有大型渲染区域的应用程序（比如谷歌地图）；
- 具有很好的打印质量和文本内容的可选字体；
- 复杂度高会减慢渲染速度（任何过度使用 DOM 的应用都不快）；
- 不适合游戏应用；

`Canvas`是画布，通过Javascript来绘制2D图形，是逐像素进行渲染的；

- 依赖分辨率；
- 不支持事件处理器；
- 能够以 .png 或 .jpg 格式保存结果图像；
- 能够实时渲染；
- 适合图像密集型的游戏，其中的许多对象会被频繁重绘；
- canvas支持的颜色比SVG多；


## SVG 基本形状

SVG 基本形状包括：矩形\<rect>、圆形\<circle>、椭圆\<ellipse>、线\<line>、折线\<polyline>、多边形\<polygon>和路径\<path>。

公用属性：

- style 属性用来定义 CSS 属性
- CSS 的 fill 属性定义矩形的填充颜色（rgb 值、颜色名或者十六进制值）
- CSS 的 stroke-width 属性定义矩形边框的宽度
- CSS 的 stroke 属性定义矩形边框的颜色
- CSS 的 fill-opacity 属性定义填充颜色透明度（合法的范围是：0 - 1）
- CSS 的 stroke-opacity 属性定义轮廓颜色的透明度（合法的范围是：0 - 1）
- CSS opacity 属性用于定义了元素的透明值 (范围: 0 到 1)

### 矩形

\<rect> 元素用于绘制矩形。

``` xml
<svg xmlns="http://www.w3.org/2000/svg" version="1.1">
  <rect x="50" y="20" rx="20" ry="20" width="150" height="150"
  style="fill:blue;stroke:pink;stroke-width:5;fill-opacity:0.1;
  stroke-opacity:0.9;opacity:0.5"/>
</svg>
```

- rect 元素的 width 和 height 属性可定义矩形的高度和宽度
- rx 和 ry 属性可使矩形产生圆角
- x 属性定义矩形的左侧位置（例如，x="0" 定义矩形到浏览器窗口左侧的距离是 0px）
- y 属性定义矩形的顶端位置（例如，y="0" 定义矩形到浏览器窗口顶端的距离是 0px）

### 圆形

\<circle> 元素用于绘制圆形。

``` xml
<svg xmlns="http://www.w3.org/2000/svg" version="1.1">
  <circle cx="100" cy="50" r="40" stroke="black"
  stroke-width="2" fill="red"/>
</svg>
```

- cx和cy属性定义圆点的x和y坐标。如果省略cx和cy，圆的中心会被设置为(0, 0)
- r属性定义圆的半径

### 椭圆

\<ellipse> 元素用于绘制椭圆。

``` xml
<svg xmlns="http://www.w3.org/2000/svg" version="1.1">
  <ellipse cx="300" cy="80" rx="100" ry="50"
  style="fill:yellow;stroke:purple;stroke-width:2"/>
</svg>
```

- CX属性定义的椭圆中心的x坐标
- CY属性定义的椭圆中心的y坐标
- RX属性定义的水平半径
- RY属性定义的垂直半径

### 线

\<line> 元素用于绘制线。

``` xml
<svg xmlns="http://www.w3.org/2000/svg" version="1.1">
  <line x1="0" y1="0" x2="200" y2="200"
  style="stroke:rgb(255,0,0);stroke-width:2"/>
</svg>
```

- x1 属性在 x 轴定义线条的开始
- y1 属性在 y 轴定义线条的开始
- x2 属性在 x 轴定义线条的结束
- y2 属性在 y 轴定义线条的结束

### 折线

\<polyline> 元素用于绘制折线。

``` xml
<svg xmlns="http://www.w3.org/2000/svg" version="1.1">
  <polyline points="0,40 40,40 40,80 80,80 80,120 120,120 120,160" style="fill:white;stroke:red;stroke-width:4" />
</svg>
```

- points 属性定义了多边形的每个顶点(相对于浏览器)，用逗号分隔

### 多边形

\<polygon> 元素用于绘制多边形。

``` xml
<svg  height="210" width="500">
  <polygon points="200,10 250,190 160,210"
  style="fill:lime;stroke:purple;stroke-width:1"/>
</svg>
```

- points 属性定义了多边形的每个顶点(相对于浏览器)，用逗号分隔

### 路径

\<path> 元素用于绘制路径，使用频率很高的。

``` xml
<svg xmlns="http://www.w3.org/2000/svg" version="1.1">
    <path d="M150 0 L75 200 L225 200 Z"></path>
</svg>
```

使用d属性来定义路径，d属性是一个字符串，包含了一系列的命令和坐标。命令可以是：

- M = moveto，移动画点到指定的坐标
- L = lineto，画一条线到指定的坐标
- H = horizontal lineto，画一条水平线到指定的x坐标
- V = vertical lineto，画一条垂直线到指定的y坐标
- C = curveto，画一条三次贝塞尔曲线到指定的坐标
- S = smooth curveto，画一条平滑的三次贝塞尔曲线到指定的坐标
- Q = quadratic Bezier curve，画一条二次贝塞尔曲线到指定的坐标
- T = smooth quadratic Bezier curveto，画一条平滑的二次贝塞尔曲线到指定的坐标
- A = elliptical Arc，画一条椭圆弧到指定的坐标
- Z = closepath，闭合路径

以上所有命令均允许小写字母。大写表示绝对定位，小写表示相对定位。

### 文本

\<text> 元素用于绘制文本。

``` xml
<svg xmlns="http://www.w3.org/2000/svg" version="1.1">
  <text x="0" y="15" fill="red">I love SVG!
    <tspan x="10" y="45">First line</tspan>
    <tspan x="10" y="70">Second line</tspan>
  </text>
</svg>
```

- x 属性定义文本的开始位置
- y 属性定义文本的基线位置
- fill 属性定义文本的颜色
- font-family 属性定义文本的字体
- font-size 属性定义文本的大小
- font-weight 属性定义文本的粗细
- text-anchor 属性定义文本的对齐方式
- transform 属性定义文本的变换
- textLength 属性定义文本的长度
- lengthAdjust 属性定义文本的长度调整方式
- dx 属性定义文本的水平偏移
- dy 属性定义文本的垂直偏移
- rotate 属性定义文本的旋转角度
- \<tspan>设置文本不同的格式和位置

## 结构元素

### group

\<g> 元素用于将多个元素组合在一起，\<g>标记就是‘group’的简写，是用来分组用的，它能把多个元素放在一组里，对\<g>标记实施的样式和渲染会作用到这个分组内的所有元素上。组内的所有元素都会继承\<g>标记上的所有属性。用\<g>定义的分组还可以使用\<use>进行复制使用。。

``` xml
<svg viewBox="0 0 95 50"
     xmlns="http://www.w3.org/2000/svg">
   <g stroke="green" fill="white" stroke-width="5">
     <circle cx="25" cy="25" r="15"/>
     <circle cx="40" cy="25" r="15"/>
     <circle cx="55" cy="25" r="15"/>
     <circle cx="70" cy="25" r="15"/>
   </g>
</svg>
```


## 参考资料

[SVG 手册](https://developer.mozilla.org/zh-CN/docs/Web/SVG/Tutorial)

[SVG 教程](https://www.runoob.com/svg/svg-tutorial.html)

[浅谈Canvas和SVG的区别](https://blog.csdn.net/m0_59310979/article/details/124870278)

[SVG 2.0](https://www.w3.org/TR/SVG2/)

[MDN - SVG 手册](http://know.webhek.com/svg/svg-home.html)

[w3c Plus - SVG](https://www.w3cplus.com/svg-tutorial)
