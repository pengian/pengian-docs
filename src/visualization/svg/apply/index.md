# SVG 应用

## SVG 如何应用到 HTML 中

- 直接在 HTML 中使用 SVG 代码
- 使用`<img>`标签引入 SVG 文件
- 使用`<object>`标签引入 SVG 文件
- 使用`<iframe>`标签引入 SVG 文件
- 使用`<embed>`标签引入 SVG 文件

示例：

```html
<!DOCTYPE html>
<html>
<body>

<h1>SVG</h1>

<svg width="100" height="100">
  <circle cx="50" cy="50" r="40" stroke="green" stroke-width="4" fill="yellow" />
</svg>

<img src="circle.svg" width="100" height="100">

<object data="circle.svg" width="100" height="100"></object>

<iframe src="circle.svg" width="100" height="100"></iframe>

<embed src="circle.svg" width="100" height="100"></embed>

</body>
</html>
```

## SVG 如何应用到 CSS 中

- 使用`background-image`属性引入 SVG 文件
- 使用`background`属性引入 SVG 文件

示例：

```html
<!DOCTYPE html>
<html>
  <head>
    <style>
      div {
        width: 100px;
        height: 100px;
        background-image: url("circle.svg");
      }
    </style>
  </head>
  <body>
    <h1>SVG</h1>

    <div></div>
  </body>
</html>
```

## SVG clip-path

clip-path 是[CSS Marking](https://www.w3.org/TR/2014/WD-css-masking-1-20140213/)的一部分，可以通过不同形状函数的指定路径来剪切元素(svg元素或HTML元素)，搭配svg可以实现更灵活的剪切。

剪切这个概念是相对的，实际上是通过指定路径来显示元素的一部分，剩余部分不显示。

示例：

:::demo
svg/clipPath
:::

### 文字裁剪

clip-path 属性可以通过 SVG 路径来剪切文字。

示例：

:::demo
svg/textClipPath
:::

## SVG mask

mask 属性可以通过 SVG 路径来遮罩元素。

示例：

:::demo
svg/mask
:::

## SVG filter

filter 属性可以通过 SVG 路径来过滤元素。

示例：

:::demo
svg/filter
:::

## SVG 文字效果

SVG的文字文本text、textpath标签可以附加相关属性来实现文字效果。

示例：

:::demo
svg/textpath
:::

## SVG线条动画

SVG的线条可以通过stroke-dasharray属性来实现动画效果，原理是通过改变stroke-dasharray的值来改变线条的显示，从而实现动画效果。

`stroke-dasharray: dash, gap`，dash 表示线条的长度，gap 表示线条之间的间隔，当 `dash` 和 `gap` 的值相等时，表示线条是实线，当 dash 和 gap 的值不相等时，表示线条是虚线，此时，我们实现动画效果的原理就是改变 dash 和 gap 的值，用dash来充当原本的线条，通过改变`stroke-dashoffset`来控制 dash 的偏移量，使得`dash`有个渐进的效果，从而实现动画效果。

示例：

:::demo
svg/lineAnimation
:::


## 参考

- [CSS和SVG中的剪切](https://www.w3cplus.com/css3/css-svg-clipping.html)
