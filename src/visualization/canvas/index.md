# Canvas

`<canvas>` 元素可被用来通过 JavaScript（[Canvas API](https://developer.mozilla.org/zh-CN/docs/Web/API/Canvas_API) 或 [WebGL API](https://developer.mozilla.org/zh-CN/docs/Web/API/WebGL_API)）绘制图形及图形动画。

标签固有属性：

- width，占用空间的宽度，以 CSS 像素（px）表示，默认为 300。
- height，占用空间的高度，以 CSS 像素（px）表示，默认为 150。

::: tip

Canvas 标签是 H5 新特性，兼容性良好(哪怕是 IE9)，当然，你可以在标签内嵌套提示信息，以便在不支持的浏览器中显示。

关于 canvas 标签画布的大小，可以通过 html 标签属性来设置，也可以通过 CSS 来设置，但是，如果你通过 CSS 设置了宽高，那么，你就不能再通过 width 和 height 属性来设置了，否则，你设置的 CSS 宽高将会被覆盖，建议使用 html 属性设置(CSS 设置可能存在图形变形问题)。
:::
