## 定时更新

实现定时更新功能，总体上来说，有两种实现方案，分别是定时更新 option、使用 timeline 自动播放，两种方式的应用场景不一，一般来说，定时更新 option 比较常用，具体实现思路如下：

- 定时更新 option：设置定时器，定时拉取服务端，通过 setOption 更新数据，缺点是旧数据需要自行保存；
- 使用 timeline 自动播放：提前拉取所有数据，通过设置 timeline 自动播放来切换不同 option 数据；

![](https://pengian-1305462289.cos.ap-nanjing.myqcloud.com/img/ae40823b-1c84-4f45-90b4-cbaf46157704.gif)

## 动态排序

实现动态排序需要 echarts 5.x 以上，使用新增的 `realtimeSort`;

![](https://pengian-1305462289.cos.ap-nanjing.myqcloud.com/img/471730aa-a584-4922-b879-6f750b2902ee.gif)

## 圆角图

### 圆角饼图

这种圆角图其实就是饼图加上`radius`设置成环形图，之后给图形设置上边框圆角(itemStyle.borderRadius)

![](https://pengian-1305462289.cos.ap-nanjing.myqcloud.com/img/20220527094203.png)

```js
var option = {
  ...,
  series: [
    {
      name: "Access From",
      type: "pie",
      radius: ["40%", "70%"],
      avoidLabelOverlap: false,
      itemStyle: {
        borderRadius: 10,
        borderColor: "#fff",
        borderWidth: 2,
      },
      ...,
    },
  ],
};
```

### 圆角环图

实现这类百分比的圆角环图，实现思路是使用极坐标系替代直角坐标系，画多个柱状图系列，并将他们成重合；

![](https://pengian-1305462289.cos.ap-nanjing.myqcloud.com/img/20220525164037.png)

```js
var option = {
  title: {
    text: "75%",
    textStyle: {
      color: "#48FFFF",
      fontSize: 12,
    },
    left: "center",
    top: "center",
  },
  angleAxis: {
    max: 100, // 满分
    clockwise: false, // 逆时针
    // 隐藏刻度线
    axisLine: {
      show: false,
    },
    axisTick: {
      show: false,
    },
    axisLabel: {
      show: false,
    },
    splitLine: {
      show: false,
    },
  },
  radiusAxis: {
    type: "category",
    // 隐藏刻度线
    axisLine: {
      show: false,
    },
    axisTick: {
      show: false,
    },
    axisLabel: {
      show: false,
    },
    splitLine: {
      show: false,
    },
  },
  polar: {
    center: ["50%", "50%"],
    radius: "140%", //图形大小
  },
  series: [
    {
      type: "bar",
      data: [
        {
          name: "作文得分",
          value: 70,
          itemStyle: {
            normal: {
              color: {
                // 完成的圆环的颜色
                colorStops: [
                  {
                    offset: 0,
                    color: "#48FFFF", // 0% 处的颜色
                  },
                  {
                    offset: 1,
                    color: "#48FFFF", // 100% 处的颜色
                  },
                ],
              },
            },
          },
        },
      ],
      coordinateSystem: "polar",
      roundCap: true,
      barWidth: 25,
      barGap: "-100%", // 两环重叠
      radius: ["49%", "52%"],
      z: 2,
    },
    {
      // 灰色环
      type: "bar",
      data: [
        {
          value: 100,
          itemStyle: {
            color: "#3B4068",
          },
        },
      ],
      coordinateSystem: "polar",
      roundCap: true,
      barWidth: 30,
      barGap: "-110%", // 两环重叠
      radius: ["48%", "53%"],
      z: 1,
    },
  ],
};
```
