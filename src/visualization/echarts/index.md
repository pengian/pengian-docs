## 概述

本文涉及的 echarts 相关知识以`echarts 5.x`版本为主，如某些配置信息不符，具体配置项信息请以官网对应版本为准；

## Echarts 组件介绍

Echarts 常用组件包括标题、提示框、工具栏、图例、时间轴、数据区域缩放、网格、坐标轴、数据系列、全局字体样式等等；

### 标题 title

标题 title 分为主标题(text)和副标题(subtext)，两者均有丰富的样式属性，还支持设置超链接，更是支持在 textStyle 中设置自定义富文本样式，具体配置[文档](https://echarts.apache.org/zh/option.html#title)

![echarts-title](https://pengian-1305462289.cos.ap-nanjing.myqcloud.com/img/20220517235533.png)

### 图例组件 legend

图例组件 legend 展现了不同系列的标记(symbol)，颜色和名字。可以通过点击图例控制哪些系列不显示，具体配置[文档](https://echarts.apache.org/zh/option.html#legend)。
当不同系列过多的时候，可以通过指定`type: 'scroll'`来设置图例组件滚动，通过设置`orient: 'vertical'`，可以令 legend 垂直排列。
![echats-legend](https://pengian-1305462289.cos.ap-nanjing.myqcloud.com/img/20220518063939.png)

```js
{
  legend: {
    data: ['销量', '产量'],
    left: 'right',
  },
  ...,
  series: [
    {
      name: '销量',
      ...
    },
    {
      name: '产量',
      ...
    }
  ]
}
```

::: tip
注意，legend 的`data`为 legend 组件的单例，每个数组项可简写为字符串(每个 legend 通过这个 name 一一对应 series 每个系列图的 name)，也可写成对象的格式`{name: '销量', icon: 'circle', ...}`
:::

### 提示框 tooltip

提示框 tooltip，在数据边上显示数据情况的弹框，其作用是在何时的时机向用户提供相关信息，具体配置[文档](https://echarts.apache.org/zh/option.html#tooltip)。

![echarts-tooltip](https://pengian-1305462289.cos.ap-nanjing.myqcloud.com/img/20220518065747.png)

- trigger：触发类型，可选参数有 item(鼠标悬浮图形项触发)、axis(鼠标悬浮坐标轴触发)、none(不触发)；
- formatter：提示框浮层内容格式器，一般使用模板字符串，模板变量有{a}、{b}、{c}、{d}、{e}，分别代表系列名、数据名、数据值等等，其中变量{a}、{b}、{c}、{d}在不同图表类型下代表的数据含义不同；
  - 折线图、柱形图、K 线图：系列名、类目值、数值、无；
  - 散点图：系列名、数据名称、数值数组、无；
  - 地图：系列名、区域名称、合并数值、无；
  - 饼图、仪表图、漏斗图：系列名、数据项名称、数值、百分比；

### 工具栏 toolbox

工具栏 toolbox，内置有导出图片，数据视图，动态类型切换，数据区域缩放，重置五个工具，具体配置[文档](https://echarts.apache.org/zh/option.html#toolbox)。

![echarts-toolbox](https://pengian-1305462289.cos.ap-nanjing.myqcloud.com/img/20220518071410.png)

可以通过`feature`配置项为每种工具进行相关的配置。

### 时间轴 timeline

时间轴 timeline，区别于其它组件，这个组件较为特殊，其作用是提供了在多个 ECharts option 间进行切换、播放等操作的功能，具体配置[文档](https://echarts.apache.org/zh/option.html#timeline)。

![echarts-timeline](https://pengian-1305462289.cos.ap-nanjing.myqcloud.com/img/65756778-14ee-4745-9889-0a8c4e53278b.gif)

因为使用时，存在多个 option，所以 echarts 将通用 option 成为原子 option(baseOption)，将使用 timeline 展示的 option 成为复合 option(options)；

```js
// 原子option在配置项中为baseOption，需要设置timeline
// 复合option存在options配置项中
var option = {
  baseOption: {
    timeline: {
      data: ['2021年', '2022年'],
    },
    xAxis: {...},
    ...,
    series: {...},
  },
  options:[
    {
      title: {
        text: '2021年度销量表',
      },
      series: {
        data: [...],
      }
    },
    {
      title: {
        text: '2022年度销量表',
      },
      series: {
        data: [...],
      }
    },
  ]
}
```

### 数据区域缩放 dataZoom

dataZoom 组件用于区域缩放，从而能自由关注细节的数据信息，或者概览数据整体，或者去除离群点的影响，具体配置[文档](https://echarts.apache.org/zh/option.html#dataZoom)。

![echarts-dataZoom](https://pengian-1305462289.cos.ap-nanjing.myqcloud.com/img/20220518092721.png)

现在支持这几种类型的 dataZoom 组件：

- 内置型数据区域缩放组件（dataZoomInside）：内置于坐标系中，使用户可以在坐标系上通过鼠标拖拽、鼠标滚轮、手指滑动（触屏上）来缩放或漫游坐标系。
- 滑动条型数据区域缩放组件（dataZoomSlider）：有单独的滑动条，用户在滑动条上进行缩放或漫游。
- 框选型数据区域缩放组件（dataZoomSelect）：提供一个选框进行数据区域缩放。即 toolbox.feature.dataZoom，配置项在 toolbox 中。

```js
var option = {
  dataZoom: [
    {
      id: 'xAxisDataZoom',
      type: 'slider',
      xAxisIndex: [0],
      filterMode: 'filter',
    }
  ],
  ...,
}
```

#### 滑块条类型数据区域缩放组件

通过设置 dataZoom 的`type: 'slider'`，滑块条类型数据区域缩放组件常用的参数如下：

- xAxisIndex：指定控制的 x 轴，当 xAxis 中配置多个 xAxis 是，从 Index0 开始，指定该缩放组件影响哪些 x 轴，传数组 index(传多个 index 时，表示范围)；
- yAxisIndex：同 xAxisIndex，控制 y 轴；
- filterMode：过滤模式，dataZoom 数据窗口缩放的实质是数据过滤，该参数控制过滤时，数据显示的行为，可选值如下；
  - filter：过滤窗口外的数据，当可视化有多个轴时，会影响其它轴的数据范围，对于数据，只要一个维度在窗口外，就会被过滤掉；
  - weakFilter：过滤窗口外的数据，当可视化有多个轴时，会影响其它轴的数据范围，对于数据，只有当其所有维度都在窗口同侧外，才会被过滤掉；
  - empty：不会影响其它轴的数据范围；
  - none：不过滤数据，只会改变数轴范围；

#### 内置型数据区域缩放组件

通过设置 dataZoom 的`type: 'inside'`，实质上是将滑块条操作改为鼠标滚轮操作，滑块条不显示；

#### 框选型数据区域缩放组件

即 toolbox.feature.dataZoom，配置项在 toolbox 中，设置后，在 toolbox 中多了两个按钮“区域缩放”、“区域缩放还原”，点击“区域缩放”按钮，在可视化图中划出一个区域，即可对数据进行缩放，点击“区域缩放还原”即可复原；

### 网格 grid

网格 grid，echarts 图例中，单个图例默认一个 grid，单个 grid 内最多可以放置上下两个 X 轴，左右两个 Y 轴。可以在网格上绘制折线图，柱状图，散点图（气泡图），如果想一个图例防止多个图形(如下图)，怎么做到呢？其实就是将默认的单个 grid 设置成多个 grid，并控制 grid 的位置宽高等属性来实现效果，具体配置[文档](https://echarts.apache.org/zh/option.html#grid)。

![echarts-grid](https://pengian-1305462289.cos.ap-nanjing.myqcloud.com/img/20220520070840.png)

基本配置如下：

```js
var option = {
  ...,
  grid: [
    { left: '7%', top: '7%', width: '38%', height: '38%' },
    { right: '7%', top: '7%', width: '38%', height: '38%' },
    { left: '7%', bottom: '7%', width: '38%', height: '38%' },
    { right: '7%', bottom: '7%', width: '38%', height: '38%' }
  ],
  xAxis: [
    { gridIndex: 0, min: 0, max: 20 },
    { gridIndex: 1, min: 0, max: 20 },
    { gridIndex: 2, min: 0, max: 20 },
    { gridIndex: 3, min: 0, max: 20 }
  ],
  yAxis: [
    { gridIndex: 0, min: 0, max: 15 },
    { gridIndex: 1, min: 0, max: 15 },
    { gridIndex: 2, min: 0, max: 15 },
    { gridIndex: 3, min: 0, max: 15 }
  ],
}
```

### 坐标轴 xAxis、yAxis

坐标轴 axis，在常用的直角坐标轴中，xAxis、yAxis 是最常使用到的，其常用配置参数如下：

- name：指定坐标轴名称，可搭配 nameLocation(start、end、center)来设定坐标轴名称位置；
- type：指定坐标轴的类型，可选参数有四种：value(表示数值类型的轴)、category(表示类别类型的轴，此时 series.data 的数据才可以为字符串类型)、time(表示时间类型的轴，适用于连续时间序列数据)、log(表示对数类型的轴，适用于对数数据)；
- gridIndex：指定坐标轴所处的网格；
- data：用于指定该坐标轴的数据，默认情况下，series.data 的格式最终会转变为二维数组[[0, 99],[1, 53], ...]，这样的[[x 轴, y 轴, 其它轴...], [x 轴, y 轴, 其它轴...], ...]的格式，未指定坐标轴`type: 'category'`的时候，series.data 都会默认映射坐标轴`data`数组的下标，进而取值；
- axisLine：用来配置轴线两端的箭头，轴线的样式等；
- axisTick：用来配置刻度线的长度，样式等；
- axisLabel：用来配置文字对齐方式，自定义刻度标签内容等；

### 系列 series

一个图例中可以有多个系列，每个系列又可以包含多个数据，在不同类型的图形中，series 有着不同的属性，具体配置[文档](https://echarts.apache.org/zh/option.html#series)；

<img src="https://pengian-1305462289.cos.ap-nanjing.myqcloud.com/img/20220520094440.png" width="300" />

图示的就是同系列下包含的两种类型图形，图形的数据各自独立；

series 通用设置有：

- type：设置系列的图形类型，常用的参数有 bar、line、pie 等等；
- name：tooltip 的时候会显示；
- xAxisIndex、yAxisIndex：指定 x、y 轴的数组 index 索引；
- label：数据节点标签，一般用作显示数据值，可设置 formatter 来修改显示内容；

![echarts-label](https://pengian-1305462289.cos.ap-nanjing.myqcloud.com/img/20220520105323.png)

### 颜色组 color

调色盘颜色列表。如果系列没有设置颜色，则会依次循环从该列表中取颜色作为系列颜色。 默认为：`['#5470c6', '#91cc75', '#fac858', '#ee6666', '#73c0de', '#3ba272', '#fc8452', '#9a60b4', '#ea7ccc']`
在所有配置的颜色都支持基本的`HEX`、`RGB`、`RGBA`格式，还有渐变色和纹理，其中渐变色支持两种写法，具体写法如下：

```js
// 线性渐变，前四个参数分别是 x0, y0, x2, y2, 范围从 0 - 1，相当于在图形包围盒中的百分比，如果 globalCoord 为 `true`，则该四个值是绝对的像素位置
color: [
  {
    type: "linear",
    x: 0,
    y: 0,
    x2: 0,
    y2: 1,
    colorStops: [
      {
        offset: 0,
        color: "red", // 0% 处的颜色
      },
      {
        offset: 1,
        color: "blue", // 100% 处的颜色
      },
    ],
    global: false, // 缺省为 false
  },
  // 另一种渐变色写法(echarts文档上没体现)，参数格式同上
  new echarts.graphic.LinearGradient(0, 0, 0, 1, [
    { offset: 1, color: "red" },
    { offset: 0, color: "blue" },
  ]),
  // 径向渐变，前三个参数分别是圆心 x, y 和半径，取值同线性渐变
  {
    type: "radial",
    x: 0.5,
    y: 0.5,
    r: 0.5,
    colorStops: [
      {
        offset: 0,
        color: "red", // 0% 处的颜色
      },
      {
        offset: 1,
        color: "blue", // 100% 处的颜色
      },
    ],
    global: false, // 缺省为 false
  },
  // 纹理填充
  {
    image: imageDom, // 支持为 HTMLImageElement, HTMLCanvasElement，不支持路径字符串
    repeat: "repeat", // 是否平铺，可以是 'repeat-x', 'repeat-y', 'no-repeat'
  },
];
```

### media 媒体查询

echarts 提供类似于 css 媒体查询的能力，用作实现移动端下不同尺寸的 option 适配问题；

### dataset 数据集

数据设置在 数据集（dataset）中，会有这些好处：

能够贴近数据可视化常见思维方式：（I）提供数据，（II）指定数据到视觉的映射，从而形成图表。
数据和其他配置可以被分离开来。数据常变，其他配置常不变。分开易于分别管理。
数据可以被多个系列或者组件复用，对于大数据量的场景，不必为每个系列创建一份数据。
支持更多的数据的常用格式，例如二维数组、对象数组等，一定程度上避免使用者为了数据格式而进行转换。

#### 把数据集（ dataset ）的行或列映射为系列（series）

用户可以使用 seriesLayoutBy 配置项，改变图表对于行列的理解。seriesLayoutBy 可取值：

'column': 默认值。系列被安放到 dataset 的列上面(x 轴取列上的值)。
'row': 系列被安放到 dataset 的行上面(x 轴取行上的值)。

#### 数据到图形的映射（ series.encode ）

```js
var option = {
  dataset: {
    source: [
      ["score", "amount", "product"],
      [89.3, 58212, "Matcha Latte"],
      [57.1, 78254, "Milk Tea"],
      [74.4, 41032, "Cheese Cocoa"],
      [50.1, 12755, "Cheese Brownie"],
      [89.7, 20145, "Matcha Cocoa"],
      [68.1, 79146, "Tea"],
      [19.6, 91852, "Orange Juice"],
      [10.6, 101852, "Lemon Juice"],
      [32.7, 20112, "Walnut Brownie"],
    ],
  },
  xAxis: {},
  yAxis: { type: "category" },
  series: [
    {
      type: "bar",
      encode: {
        // 将 "amount" 列映射到 X 轴。
        x: "amount",
        // 将 "product" 列映射到 Y 轴。
        y: "product",
      },
    },
  ],
};
```

### 极坐标 polar

`echarts`提供的二维坐标系中，包含极坐标系(用于散点图、折线图、柱形图等等)，最简单的，我们只需要配置`polar: {}`，即可将直角坐标系转换为极坐标系，直角坐标系下的 x、y 轴部分配置仍然生效，多出的配置是极坐标系下对应的角度轴`angleAxis`、半径轴`radiusAxis`；
极坐标常见的用法是，将柱形图转换成极坐标柱形图

#### 使用 transform 进行数据转换

在 echarts 中，数据转换是依托于数据集（dataset）来实现的. 我们可以设置 dataset.transform 来表示，此 dataset 的数据，来自于此 transform 的结果。

```js
var option = {
  dataset: [
    {
      // 这个 dataset 的 index 是 `0`。
      source: [
        ['Product', 'Sales', 'Price', 'Year'],
        ['Cake', 123, 32, 2011],
        ['Cereal', 231, 14, 2011],
        ['Cake', 143, 30, 2012],
        ['Cereal', 201, 19, 2012],
        ['Cake', 153, 28, 2013],
        ['Biscuit', 92, 39, 2013],
        ['Cake', 223, 29, 2014],
        ['Biscuit', 72, 24, 2014]
      ]
      // id: 'a'
    },
    {
      // 这个 dataset 的 index 是 `1`。
      // 这个 `transform` 配置，表示，此 dataset 的数据，来自于此 transform 的结果。
      transform: {
        type: 'filter',
        config: { dimension: 'Year', value: 2011 }
      }
      // 我们还可以设置这些可选的属性： `fromDatasetIndex` 或 `fromDatasetId`。
      // 这些属性，指定了，transform 的输入，来自于哪个 dataset。例如，
      // `fromDatasetIndex: 0` 表示输入来自于 index 为 `0` 的 dataset 。又例如，
      // `fromDatasetId: 'a'` 表示输入来自于 `id: 'a'` 的 dataset。
      // 当这些属性都不指定时，默认认为，输入来自于 index 为 `0` 的 dataset 。
    },
    {
      // 这个 dataset 的 index 是 `2`。
      // 同样，这里因为 `fromDatasetIndex` 和 `fromDatasetId` 都没有被指定，
      // 那么输入默认来自于 index 为 `0` 的 dataset 。
      transform: {
        // 这个类型为 "filter" 的 transform 能够遍历并筛选出满足条件的数据项。
        type: 'filter',
        // 每个 transform 如果需要有配置参数的话，都须配置在 `config` 里。
        // 在这个 "filter" transform 中，`config` 用于指定筛选条件。
        // 下面这个筛选条件是：选出维度（ dimension ）'Year' 中值为 2012 的所有
        // 数据项。
        config: { dimension: 'Year', value: 2012 }
      }
    },
    {
      // 这个 dataset 的 index 是 `3`。
      transform: {
        type: 'filter',
        config: { dimension: 'Year', value: 2013 }
      }
    }
  ],
  series: [
    {
      type: 'pie',
      radius: 50,
      center: ['25%', '50%'],
      // 这个饼图系列，引用了 index 为 `1` 的 dataset 。也就是，引用了上述
      // 2011 年那个 "filter" transform 的结果。
      datasetIndex: 1
    },
    ...
  ]
};
```

transform 可以被链式声明，这是一个语法糖。

```js
option = {
  dataset: [
    {
      source: [
        // 原始数据
      ],
    },
    {
      // 几个 transform 被声明成 array ，他们构成了一个链，
      // 前一个 transform 的输出是后一个 transform 的输入。
      transform: [
        {
          type: "filter",
          config: { dimension: "Product", value: "Tofu" },
        },
        {
          type: "sort",
          config: { dimension: "Year", order: "desc" },
        },
      ],
    },
  ],
  series: {
    type: "pie",
    // 这个系列引用上述 transform 的结果。
    datasetIndex: 1,
  },
};
```

## 常见图形用法和注意点

### 折线图

#### 属性介绍

- smooth：设置为 true 之后，数据折点会变成平滑过渡；

  ![](https://pengian-1305462289.cos.ap-nanjing.myqcloud.com/img/20220523094347.png)

- stack：数据堆叠，同个类目轴上系列配置相同的 stack 值后，后一个系列的值会在前一个系列的值上相加；

  ![](https://pengian-1305462289.cos.ap-nanjing.myqcloud.com/img/20220523094239.png)

- areaStyle：区域填充样式。设置后显示成区域面积图；

  ![](https://pengian-1305462289.cos.ap-nanjing.myqcloud.com/img/20220523094305.png)

- step：设置 series 的 step 即可实现阶梯线图；

![](https://pengian-1305462289.cos.ap-nanjing.myqcloud.com/img/20220523094449.png)

### 柱状图

#### 属性介绍

- stack：数据堆叠，同个类目轴上系列配置相同的 stack 值后，后一个系列的值会在前一个系列的值上相加；

![](https://pengian-1305462289.cos.ap-nanjing.myqcloud.com/img/20220523094513.png)

- axisPointer：axisPointer 设置`type: 'shadow'`使得鼠标悬停柱子的时候可以显示阴影效果；

![](https://pengian-1305462289.cos.ap-nanjing.myqcloud.com/img/20220523094604.png)

- barGap：不同系列的柱间距离，为百分比（如 '30%'，表示柱子宽度的 30%，设置 barGap 为-100%时，会与下系列重叠）；
- barCategoryGap：同一系列的柱间距离，默认为类目间距的 20%，可设固定值；

### 饼图

#### 属性介绍

- radius：通过设置 series 的`radius: ['50%', '70%']`即可将饼图设置为环形图的样式

## Echarts 高级功能

### Echarts 实例 Action

提到 Echarts 实例的方法，大家第一时间应该会是想到 setOption 方法，其实除了这个常用的设置方法外，实例还提供挺多有趣的方法供我们调用，具体配置[文档](https://echarts.apache.org/zh/api.html#echartsInstance)；

#### 设置

- setOption 参数格式：
  `(option: Object, notMerge?: boolean, lazyUpdate?: boolean) or (option: Object, opts?: { notMerge?: boolean; replaceMerge?: string | string[]; lazyUpdate?: boolean; }) `，可见默认情况下每次调用都是 merge 的形式来更新配置；
- getOption 获取实例当前的 option 配置；
- appendDtata，增量加载数据，但不是所有图形都支持(目前只支持散点图、线图，且只有 series.data 格式支持)；

#### dom 相关

- getWidth，获取实例宽度；
- getHeight，获取实例高度；
- getDom，获取实例 dom 节点；
- resize，Echarts 新建的实例中，设置实例挂载的 dom 节点为效应式宽高(vw/vh、百分比之类的)，当窗口变动时，可监听 window 的 resize 事件，再调用实例的 resize 事件，重新渲染；
- getDataUrL，导出图表图片，返回一个 base64 的 URL，可以设置为 Image 的 src；
- getConnectedDataURL，导出联动的图表图片，返回一个 base64 的 url，可以设置为 Image 的 src。导出图片中每个图表的相对位置跟容器的相对位置有关

#### loading 效果

Echarts 新建的实例中，通过调用 showLoading、hideLoading 方法来实现加载动画；

#### 实例销毁

- clear，该方法用作清除实例上的图形；
- dispose，销毁实例；
- isDispose，判断实例是否被销毁；

#### 事件

- on，监听图例鼠标事件或 dispatchAction 触发的事件；
- off，解绑事件；
- dispatchAction，触发图表行为，例如图例开关 legendToggleSelect, 数据区域缩放 dataZoom，显示提示框 showTip 等等；

### Echarts 方法

具体配置[文档](https://echarts.apache.org/zh/api.html#echarts)

#### init

创建一个 ECharts 实例，返回 echartsInstance，不能在单个容器上初始化多个 ECharts 实例；

```js
(
  dom?: HTMLDivElement | HTMLCanvasElement,
  theme?: Object | string,
  opts?: {
    devicePixelRatio?: number, // 高清大屏应用下需要设置echarts渲染比例高些(高清)
    renderer?: string, // 默认下，echarts是使用canvas渲染的，如有需要可改为svg渲染
    useDirtyRect?: boolean, // 从 `5.0.0` 开始支持
    ssr?: boolean, // 从 `5.3.0` 开始支持
    width?: number | string,
    height?: number | string,
    locale?: string, // 从 `5.0.0` 开始支持
  }
) => ECharts;
```

- dom

实例容器，一般是一个具有高宽的 DIV 元素。只有在设置 opts.ssr 开启了服务端渲染后该参数才是可选。

也支持直接使用 canvas 元素作为容器，这样绘制完图表可以直接将 canvas 作为图片应用到其它地方，例如在 WebGL 中作为贴图，这跟使用 getDataURL 生成图片链接相比可以支持图表的实时刷新。

- theme

应用的主题。可以是一个主题的配置对象，也可以是使用已经通过 echarts.registerTheme 注册的主题名称。参见 ECharts 中的样式简介。

- opts

附加参数。有下面几个可选项：

- devicePixelRatio 设备像素比，默认取浏览器的值 window.devicePixelRatio。
- renderer 渲染模式，支持'canvas'或者'svg'。参见 使用 Canvas 或者 SVG 渲染。
- ssr 是否使用服务端渲染，只有在 SVG 渲染模式有效。开启后不再会每帧自动渲染，必须要调用 renderToSVGString 方法才能得到渲染后 SVG 字符串。
- useDirtyRect 是否开启脏矩形渲染，只有在 Canvas 渲染模式有效，默认为 false。参见 ECharts 5 新特性。
- width 可显式指定实例宽度，单位为像素。如果传入值为 null/undefined/'auto'，则表示自动取 dom（实例容器）的宽度。
- height 可显式指定实例高度，单位为像素。如果传入值为 null/undefined/'auto'，则表示自动取 dom（实例容器）的高度。
- locale 使用的语言，内置 'ZH' 和 'EN' 两个语言，也可以使用 echarts.registerLocale 方法注册新的语言包。目前支持的语言见 src/i18n。

如果不指定主题，也需在传入 opts 前先传入 null，如：

const chart = echarts.init(dom, null, {renderer: 'svg'});
注： 如果容器是隐藏的，ECharts 可能会获取不到 DIV 的高宽导致初始化失败，这时候可以明确指定 DIV 的 style.width 和 style.height，或者在 div 显示后手动调用 resize 调整尺寸。

在使用服务端渲染的模式下，必须通过 opts.width 和 opts.height 设置高和宽。

#### connect

配合 echartsInstance.group 使 多个图表实例实现联动；

```js
// 分别设置每个实例的 group id
chart1.group = "group1";
chart2.group = "group1";
echarts.connect("group1");
// 或者可以直接传入需要联动的实例数组
echarts.connect([chart1, chart2]);
```

使用 disconnect 方法来解除联动；

#### getInstanceByDom、dispose

- getInstanceByDom：获取 dom 容器上的实例。
- dispose：销毁实例，实例销毁后无法再被使用。

```js
const myChart: echarts = echarts.getInstanceByDom(target: HTMLDivElement|HTMLCanvasElement);
echats.dispose(target: ECharts|HTMLDivElement|HTMLCanvasElement);
```

#### use

使用组件，配合新的按需引入的接口使用；

```js
// 引入 echarts 核心模块，核心模块提供了 echarts 使用必须要的接口。
import * as echarts from "echarts/core";
// 引入柱状图图表，图表后缀都为 Chart
import { BarChart } from "echarts/charts";
// 引入直角坐标系组件，组件后缀都为 Component
import { GridComponent } from "echarts/components";
// 引入 Canvas 渲染器，注意引入 CanvasRenderer 或者 SVGRenderer 是必须的一步
import { CanvasRenderer } from "echarts/renderers";

// 注册必须的组件
echarts.use([GridComponent, BarChart, CanvasRenderer]);
```

#### registerMap

注册可用的地图，只在 geo 组件或者 map 图表类型中使用，使用 getMap 可获取已注册的地图；

#### registerTheme

注册主题，用于初始化实例的时候指定，[主题构建工具地址](https://echarts.apache.org/zh/theme-builder.html)。

#### registerLocale

注册语言包(从 5.0.0 开始支持)，用于初始化实例的时候指定。语言包格式见 src/i18n/langEN.ts；

### Echarts 行为(Action)

ECharts 中支持的图表行为，通过 dispatchAction 触发。

#### 高亮/取消高亮

highlight/downplay，高亮指定的系列/数据等等；

```js
// 如果要高亮系列，如需要高亮地图，需要把series改为geo：
dispatchAction({
    type: 'highlight',

    // 用 index 或 id 或 name 来指定系列。
    // 可以使用数组指定多个系列。
    seriesIndex?: number | number[],
    seriesId?: string | string[],
    seriesName?: string | string[],

    // 数据项的 index，如果不指定也可以通过 name 属性根据名称指定数据项
    dataIndex?: number | number[],
    // 可选，数据项名称，在有 dataIndex 的时候忽略
    name?: string | string[],
});

dispatchAction({
    type: 'downplay',

    // 用 index 或 id 或 name 来指定系列。
    // 可以使用数组指定多个系列。
    seriesIndex?: number | number[],
    seriesId?: string | string[],
    seriesName?: string | string[],

    // 数据项的 index，如果不指定也可以通过 name 属性根据名称指定数据项
    dataIndex?: number | number[],
    // 可选，数据项名称，在有 dataIndex 的时候忽略
    name?: string | string[],
})
```

#### select

选中指定的数据(从 5.0.0 开始支持)；选中数据会使用 [select](https://echarts.apache.org/zh/option.html#series-line.select) 配置的样式；
相关 action 还有 unselect、toggleSelected；

#### legend

操作图例组件相关的行为，必须引入图例组件后才能使用；
相关 action 有 legendSelect、legendUnSelect、legendToggleSelect、legendAllSelect、legendInverseSelect、legendScroll；

#### toolTip

提示框组件相关的行为，必须引入提示框组件后才能使用；
相关 action 有 showTip、hideTip；

#### dataZoom

数据区域缩放组件相关的行为，必须引入数据区域缩放组件后才能使用；
相关 action 有 dataZoom(区域缩放)、takeGlobalCursor(启动或关闭 toolbox 中 dataZoom 的刷选状态)；

#### timeline

时间轴组件相关的行为，必须引入时间轴组件后才能使用；
相关 action 有 timelineChange(设置当前时间点)、timelinePlayChange(切换播放状态)；

#### toolbox

工具栏组件相关的行为，必须引入工具栏组件后才能使用；
相关 action 有 restore(重置 option)；

### Echarts 事件

在 ECharts 中主要通过 on 方法添加事件处理函数；

ECharts 中的事件分为两种，一种是鼠标事件，在鼠标点击某个图形上会触发，还有一种是 调用 dispatchAction 后触发的事件；
鼠标事件包括 'click'、'dblclick'、'mousedown'、'mousemove'、'mouseup'、'mouseover'、'mouseout'、'globalout'、'contextmenu';
