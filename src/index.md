---
layout: home

hero:
  name: Pengian Docs
  actions:
    - theme: brand
      text: Get Started
      link: /vue/vue2/
    - theme: alt
      text: View on Repository
      link: https://gitee.com/pengian/pengian-docs.git
---
