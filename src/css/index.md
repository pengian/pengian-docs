## 使用 padding 撑起锚点

 在页面中使用锚点跳转指定标题的需求比较常见，点击超链接，锚点标题会滚动到页面顶部，但有时候页面顶部会有粘顶导航栏，这种情况下，会导致锚点标题被遮挡；

 在不影响页面布局的情况下，我们可以使用内联元素的 padding 来撑起锚点，如下:

```html
<h3><span id="head" style="margin-top: 50px">我是标题</span></h3>
```

 除了上述使用内联元素的padding来撑起锚点，我们还可以利用margin的负值搭配padding来实现锚点的撑起

```html
<h3 id="head" style="margin-top: -50px;padding-top:50px">
    我是标题
</h3>
```

参考自：《css 世界》-温和的 padding 属性



## 文字溢出隐藏

### 单行文字

```css
.text {
    width: 20em;
    overflow: hidden;
    text-overflow: ellipsis;
    white-space: nowrap;
    word-break: break-all;
}

```

### 多行文字

```css
.wrap2 {
    width: 200px;
    border: 2px solid #000;

    display: -webkit-box;
    -webkit-box-orient: vertical;
    -webkit-line-clamp: 2;
    text-overflow: ellipsis;
    overflow: hidden;
}
```

::: tip

​	利用webkit内核扩展属性

:::

```css
.wrap3 {
    width: 200px;
    border: 2px solid #000;
    line-height: 20px;
    height: 60px;
    position: relative;
    overflow: hidden;
}
.wrap3::after{
    content: "...";
    padding-left: 3px;
    padding-right: 4px;
    background-color: #fff;

    position: absolute;
    right: 0;
    bottom: 0;

}
```

::: tip

​	实现思路：

​	1.控制盒子的高度是行高的n倍

​	2.控制伪元素内部添加省略号，背景颜色和盒子背景颜色一致，定位到盒子的右下角，

​	3.内容溢出的隐藏

:::
